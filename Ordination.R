#J. Harrison
#May 24 2019
rm(list=ls())

set.seed(666)
options(scipen = 999)

#stolen from Mage
add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2, 
        function(x) 
          rgb(x[1], x[2], x[3], alpha=alpha))  
}

##################################
# Load packages and wrangle data #
##################################
library(vegan)

newdat <- read.csv("data/sample_metadata_corrected.7.20.19.csv")

otus <- read.csv("data/fungi/rarifiedZotus15k.csv")

newdat2 <- merge(newdat, 
                 otus,
                 by.x = "Indiv", 
                 by.y = "dat.Indiv",
                 all.x = T)

###################################################################
#make an ordination, color coded by diversity plot, and by species#
###################################################################

#make a distance matrix
otus_dist <- as.matrix(vegan::vegdist(newdat2[,51:length(newdat2)], method="bray"))

#do pcoa
res <- cmdscale(otus_dist, k=2, eig=T)
vegan::eigenvals(res)/sum(vegan::eigenvals(res))


d3 <- data.frame(res$points[,1], res$points[,2])

colz = RColorBrewer::brewer.pal(5,"Set1")

d3$cols = NA
d3$cols[which(newdat2$diversity=="4")] = add.alpha(colz[5], alpha=0.8)
d3$cols[which(newdat2$diversity=="12")] = add.alpha(colz[2], alpha=0.8)
d3$cols[which(newdat2$diversity=="1")] = add.alpha(colz[3], alpha=0.8)

pdf(file="./visuals/ordinationPlot.pdf", width = 10, height = 10)
#plot for all diversity plots combined

par(mfrow=c(2,2), oma = c(2,2,2,2))
plot(d3[,1],d3[,2], 
     pch=16,
     col=d3$cols, 
     
     cex = 1.8, 
     cex.lab = 1, 
     xlab="PCoA 1 (7.0% variation explained)",
     ylab = "PCoA 2 (6.2% variation explained)" )
text("a)",
     xpd=NA,
     x= -0.6,
     y = 0.6,
     cex = 1.5)
legend("topleft", 
       legend=c(1,4,12), 
       col=colz[c(5,2,3)],
       pch=16, 
       cex=1.5, 
       bty="n"
)

#################
#plot by species#
#################

#define colors
colz = RColorBrewer::brewer.pal(8,"Set1")
coul = colorRampPalette(colz)(16)
#check colors
#pie(rep(1, length(coul)), col = coul , main="") 

#assign colors
d3$cols = NA
k <- 1
for(i in unique(newdat2$spp_actual)){
  d3$cols[which(as.character(newdat2$spp_actual)==i)] <- add.alpha(coul[k], alpha=0.8)
  k <- k + 1
}

plot(d3[,1],d3[,2], 
     pch=16,
     col=d3$cols, 
     
     cex = 1.8, 
     cex.lab = 1, 
     xlab="PCoA 1 (7.0% variation explained)",
     ylab = "PCoA 2 (6.2% variation explained)" )
text("b)",
      xpd=NA,
     x= -0.6,
     y = 0.6,
     cex = 1.5)
text("All plots",
     xpd=NA,
     x= -0.36,
     y = 0.6,
     pos = 4,
     cex = 1.5)
mono <- which(newdat2$diversity == 1)

plot(d3[mono,1],d3[mono,2], 
     pch=16,
     col=d3$cols[mono], 
     cex = 1.8, 
     cex.lab = 1, 
     xlab="PCoA 1 (7.0% variation explained)",
     ylab = "PCoA 2 (6.2% variation explained)" )
text("c)",
     xpd=NA,
     x= -0.5,
     y = 0.6,
     cex = 1.5)
text("Diversity = 1",
     xpd=NA,
     x= -0.36,
     y = 0.6,
     pos = 4,
     cex = 1.5)
twv <- which(newdat2$diversity == 12)

plot(d3[twv,1],d3[twv,2], 
     pch=16,
     col=d3$cols[twv], 
     cex = 1.8, 
     cex.lab = 1, 
     xlab="PCoA 1 (7.0% variation explained)",
     ylab = "PCoA 2 (6.2% variation explained)" )
text("d)",
     xpd=NA,
     x= -0.5,
     y = 0.6,
     cex = 1.5)
text("Diversity = 12",
     xpd=NA,
     x= -0.3,
     y = 0.6,
     pos = 4,
     cex = 1.5)
dev.off()

# make legend
pdf(file="./visuals/legend_ordination.pdf")
plot(NULL)
legend("left",
       legend=levels(droplevels(newdat2$spp_actual)),
       col=coul,
       pch=16, 
       cex=1.5)
dev.off()