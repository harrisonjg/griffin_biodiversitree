rm(list=ls())

#From Mage
add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2, 
        function(x) 
          rgb(x[1], x[2], x[3], alpha=alpha))  
}
#Make a color vector, needs to be 15 or more long.
cols <- c(RColorBrewer::brewer.pal(8, "Dark2"),
          add.alpha(RColorBrewer::brewer.pal(8, "Dark2"), 0.5)
)

dat <- read.csv("data/sample_metadata_corrected.7.20.19.csv")

#sanity check, simple models
# lm(Fungalrichness ~ as.factor(diversity)
#    + dbmem_out.MEM1
#    + rep
#    + spp_actual
#    ,
#    data = dat)
# lm(dat$FungaldiversityShannon ~ as.factor(diversity)
#    + dbmem_out.MEM1
#    + rep
#    + spp_actual
#    , 
#    data = dat)
# lm(dat$FungaldiversitySimpson~ as.factor(diversity)
#    + dbmem_out.MEM1
#    + rep
#    + spp_actual
#    , 
#    data = dat)
# lmerTest::lmer(Fungalrichness ~ 
#                  as.factor(diversity)
#    + dbmem_out.MEM1
#    + rep
#    + (1|spp_actual)
#    , 
#    data = dat)

###########
#Fungi lm#
##########
resultsFUNG <- matrix(ncol = 9, 
                      nrow = 15)
k <- 1
colnames(resultsFUNG) <- c("host", "div1", "div4", "div12", "div1se", "div4se", "div12se", "p4", "p12")
for(i in unique(dat$spp_actual)){
  dat_sub <- subset(dat, spp_actual == i)
  if(length(unique(dat_sub$diversity)) > 1){
    if(length(unique(dat_sub$rep)) >= 2){
      regR <- lm(Fungalrichness ~ 
                + as.factor(diversity)
                  + ht
                 #+ dbmem_out.MEM4
                  + dbmem_out.MEM1
                  + dbmem_out.MEM2
                  + dbmem_out.MEM3
                 # + dbmem_out.MEM5
                 # + dbmem_out.MEM6
                 # + dbmem_out.MEM7
                 # + dbmem_out.MEM8
                 # + dbmem_out.MEM9
                 #  + dbmem_out.MEM10
                 + rep
                 , 
                 data = dat_sub)
    }else{
      regR <- lm(Fungalrichness ~ as.factor(diversity)
                  + ht
                 #+ dbmem_out.MEM4
                  + dbmem_out.MEM1
                  + dbmem_out.MEM2
                  + dbmem_out.MEM3
                 # + dbmem_out.MEM5
                 # + dbmem_out.MEM6
                 # + dbmem_out.MEM7
                 # + dbmem_out.MEM8
                 # + dbmem_out.MEM9
                 #  + dbmem_out.MEM10
                 #+ rep
                 , 
                 data = dat_sub)
    }
  }else{next}
  
  par(mfrow = c(2,2))
  plot(regR, which = c(1,2,3,4))
  
  #Extract intercept and add it to the diversity estimates 
  div1 <- as.numeric(coef(summary(regR))[,1][1])
  div4 <- as.numeric(coef(summary(regR))[,1][2])
  div12 <- as.numeric(coef(summary(regR))[,1][3])
  
  #Extract SE for each
  div1se <- as.numeric(coef(summary(regR))[,2][1])
  div4se <- as.numeric(coef(summary(regR))[,2][2])
  div12se <- as.numeric(coef(summary(regR))[,2][3])
  
  #extract P value
  div4_12_p <- as.numeric(coef(summary(regR))[,4][2:3])
  
  resultsFUNG[k,] <- c(as.character(i), div1, div4, div12, div1se, div4se, div12se,div4_12_p)
  k <- k + 1
  
}

#Fungal Div Shannon
resultsFUNGshannon <- matrix(ncol = 9, 
                       nrow = 15)
k <- 1
colnames(resultsFUNGshannon) <- c("host", "div1", "div4", "div12", "div1se", "div4se", "div12se", "p4", "p12")
for(i in unique(dat$spp_actual)){
  dat_sub <- subset(dat, spp_actual == i)
  if(length(unique(dat_sub$diversity)) > 1){
    if(length(unique(dat_sub$rep)) >= 2){
      regR <- lm(exp(FungaldiversityShannon) ~ as.factor(diversity)
                  + ht
                 #+ dbmem_out.MEM4
                  + dbmem_out.MEM1
                  + dbmem_out.MEM2
                  + dbmem_out.MEM3
                 # + dbmem_out.MEM5
                 # + dbmem_out.MEM6
                 # + dbmem_out.MEM7
                 # + dbmem_out.MEM8
                 # + dbmem_out.MEM9
                 #  + dbmem_out.MEM10
                 + rep
                 , 
                 data = dat_sub)
    }else{
      regR <- lm(exp(FungaldiversityShannon) ~ as.factor(diversity)
                  + ht
                 #+ dbmem_out.MEM4
                  + dbmem_out.MEM1
                  + dbmem_out.MEM2
                  + dbmem_out.MEM3
                 # + dbmem_out.MEM5
                 # + dbmem_out.MEM6
                 # + dbmem_out.MEM7
                 # + dbmem_out.MEM8
                 # + dbmem_out.MEM9
                 #  + dbmem_out.MEM10
                 #+ rep
                 , 
                 data = dat_sub)
    }
  }else{next}
  
  
  par(mfrow = c(2,2))
  plot(regR, which = c(1,2,3,4))
  
  #Extract intercept and add it to the diversity estimates 
  div1 <- as.numeric(coef(summary(regR))[,1][1])
  div4 <- as.numeric(coef(summary(regR))[,1][2])
  div12 <- as.numeric(coef(summary(regR))[,1][3])
  
  #Extract SE for each
  div1se <- as.numeric(coef(summary(regR))[,2][1])
  div4se <- as.numeric(coef(summary(regR))[,2][2])
  div12se <- as.numeric(coef(summary(regR))[,2][3])
  
  #extract P value
  div4_12_p <- as.numeric(coef(summary(regR))[,4][2:3])
  
  resultsFUNGshannon[k,] <- c(as.character(i), div1, div4, div12, div1se, div4se, div12se, div4_12_p)
  k <- k + 1
  
}

#Fungal Div Simpson
resultsFUNGsimps <- matrix(ncol = 9, 
                              nrow = 15)
k <- 1
colnames(resultsFUNGsimps) <- c("host", "div1", "div4", "div12", "div1se", "div4se", "div12se", "p4", "p12")
for(i in unique(dat$spp_actual)){
  dat_sub <- subset(dat, spp_actual == i)
  if(length(unique(dat_sub$diversity)) > 1){
    if(length(unique(dat_sub$rep)) >= 2){
      regR <- lm(1 /FungaldiversitySimpson ~ as.factor(diversity)
                  + ht
                 #+ dbmem_out.MEM4
                  + dbmem_out.MEM1
                  + dbmem_out.MEM2
                  + dbmem_out.MEM3
                 # + dbmem_out.MEM5
                 # + dbmem_out.MEM6
                 # + dbmem_out.MEM7
                 # + dbmem_out.MEM8
                 # + dbmem_out.MEM9
                 #  + dbmem_out.MEM10
                 + rep
                 , 
                 data = dat_sub)
    }else{
      regR <- lm(1 /FungaldiversitySimpson ~ as.factor(diversity)
                  + ht
                 #+ dbmem_out.MEM4
                  + dbmem_out.MEM1
                  + dbmem_out.MEM2
                  + dbmem_out.MEM3
                 # + dbmem_out.MEM5
                 # + dbmem_out.MEM6
                 # + dbmem_out.MEM7
                 # + dbmem_out.MEM8
                 # + dbmem_out.MEM9
                 #  + dbmem_out.MEM10
                 #+ rep
                 , 
                 data = dat_sub)
    }
  }else{next}
  
  par(mfrow = c(2,2))
  plot(regR, which = c(1,2,3,4))
  
  #Extract intercept and add it to the diversity estimates 
  div1 <- as.numeric(coef(summary(regR))[,1][1])
  div4 <- as.numeric(coef(summary(regR))[,1][2])
  div12 <- as.numeric(coef(summary(regR))[,1][3])
  
  #Extract SE for each
  div1se <- as.numeric(coef(summary(regR))[,2][1])
  div4se <- as.numeric(coef(summary(regR))[,2][2])
  div12se <- as.numeric(coef(summary(regR))[,2][3])
  
  #extract P value
  div4_12_p <- as.numeric(coef(summary(regR))[,4][2:3])
  
  resultsFUNGsimps[k,] <- c(as.character(i), div1, div4, div12, div1se, div4se, div12se, div4_12_p)
  k <- k + 1
}
# resultsFUNGshannon[,c(1,2,3,9)]
# p.adjust(resultsFUNGsimps[,9], method = "BH")
# p.adjust(resultsFUNG[,9], method = "BH")
# p.adjust(resultsFUNGshannon[,9], method = "BH")
write.csv(data.frame(resultsFUNGshannon, p.adjust(resultsFUNGshannon[,9], "BH")),
          file = "./biodiversitree_results/lm_div_hostspecific_shannon.csv")
write.csv(data.frame(resultsFUNGsimps, p.adjust(resultsFUNGsimps[,9], "BH")),
          file = "./biodiversitree_results/lm_div_hostspecific_simpson.csv")
write.csv(data.frame(resultsFUNG, p.adjust(resultsFUNG[,9], "BH")),
          file = "./biodiversitree_results/lm_div_hostspecific_richness.csv")

#recode intercept to zero so that when plotting everything starts at 0
resultsFUNG[,2] <- 0
resultsFUNGshannon[,2] <- 0
resultsFUNGsimps[,2] <- 0

########
pdf(width = 12,
    height = 8, 
    file = "./visuals/FungalDiversityLM_stickPlot2.pdf")
par(mfrow = c(1,3),
    oma = c(4,7,1,1),
    mar = c(2,2,1,1))
plot(NULL,
     ylim = c(-100,70),
     xlim = c(0.8,3.2),
     ylab = "",
     xpd = NA,
     cex.lab = 2,
     xlab = "",
     yaxt = "n",
     xaxt = "n",
     bty = "n"
)
mtext("Richness", 
      side = 3, 
      line = -3,
      cex = 2)
axis(side = 1, labels = c("1","4","12"),
     at = c(1,2,3),
     cex.axis = 1.8)
axis(side = 2, labels = seq(-100,70, by = 10),
     at = seq(-100,70, by = 10),
     las = 2,
     cex.axis = 1.8)
mtext("Effect of increasing diversity\nrelative to monoculture",
     side = 2,
     line = 5,
     cex = 1.5)

for(i in 1:length(resultsFUNG[,1])){
  
  lines(x = c(1,2,3),
        y = as.numeric(resultsFUNG[i,2:4]),
        col = cols[i],
        lwd = 2.5
  )
  
  # #plot CIs, first make xy coords for polygons
  # tops <- cbind(c(1,2,3), as.numeric(results[i,2:4]) + 1.96*as.numeric(results[i,5:7]))
  # bottoms <- cbind(c(1,2,3), as.numeric(results[i,2:4]) - 1.96*as.numeric(results[i,5:7]))
  # bottoms <- bottoms[nrow(bottoms):1,] #This reversal is so the polygon plots right
  # 
  # coords <- rbind(tops,
  #                 bottoms)
  # polygon(coords,
  #         col = add.alpha(cols[i],0.1),
  #         border = NA)
  
}
#Calculate averages of betas
lines(x = c(1,2,3),
      y = c(0, 
            mean(as.numeric(resultsFUNG[,3])),
            mean(as.numeric(resultsFUNG[,4]))),
      col = "black",
      lwd = 5,
      lty = 2
      )


#Shannons DIVERSITY

plot(NULL,
     ylim = c(-15,15),
     xlim = c(0.8,3.2),
     ylab = "",
     xpd = NA,
     cex.lab = 2,
     xlab = "",
     yaxt = "n",
     xaxt = "n",
     bty = "n"
)
mtext("Shannon-Weiner", 
      side = 3, 
      line = -3,
      cex = 2)
axis(side = 1, labels = c("1","4","12"),
     at = c(1,2,3),
     cex.axis = 1.8)
axis(side = 2, 
     labels = seq(-15,15, by = 5),
     at = seq(-15,15, by = 5),
     las = 2,
     cex.axis = 1.8)

for(i in 1:length(resultsFUNGshannon[,1])){
  
  lines(x = c(1,2,3),
        y = as.numeric(resultsFUNGshannon[i,2:4]),
        col = cols[i],
        lwd = 2.5
  )
  
  # #plot CIs, first make xy coords for polygons
  # tops <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) + 1.96*as.numeric(resultsS[i,5:7]))
  # bottoms <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) - 1.96*as.numeric(resultsS[i,5:7]))
  # bottoms <- bottoms[nrow(bottoms):1,] #This reversal is so the polygon plots right
  # 
  # coords <- rbind(tops,
  #                 bottoms)
  # polygon(coords,
  #         col = add.alpha(cols[i],0.1),
  #         border = NA)
  
}
#Calculate averages of betas
lines(x = c(1,2,3),
      y = c(0, 
            mean(as.numeric(resultsFUNGshannon[,3])),
            mean(as.numeric(resultsFUNGshannon[,4]))),
      col = "black",
      lwd = 5,
      lty = 2
)


#Simpsons DIVERSITY

plot(NULL,
     ylim = c(-12,12),
     xlim = c(0.8,3.2),
     ylab = "",
     xpd = NA,
     cex.lab = 2,
     xlab = "",
     yaxt = "n",
     xaxt = "n",
     bty = "n"
)
mtext("Simpson", 
      side = 3, 
      line = -3,
      cex = 2)
axis(side = 1, labels = c("1","4","12"),
     at = c(1,2,3),
     cex.axis = 1.8)
axis(side = 2, 
     labels = seq(-12,12, by = 4),
     at = seq(-12,12, by = 4),
     las = 2,
     cex.axis = 1.8)

for(i in 1:length(resultsFUNGsimps[,1])){
  
  lines(x = c(1,2,3),
        y = as.numeric(resultsFUNGsimps[i,2:4]),
        col = cols[i],
        lwd = 2.5
  )
  
  # #plot CIs, first make xy coords for polygons
  # tops <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) + 1.96*as.numeric(resultsS[i,5:7]))
  # bottoms <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) - 1.96*as.numeric(resultsS[i,5:7]))
  # bottoms <- bottoms[nrow(bottoms):1,] #This reversal is so the polygon plots right
  # 
  # coords <- rbind(tops,
  #                 bottoms)
  # polygon(coords,
  #         col = add.alpha(cols[i],0.1),
  #         border = NA)
  
}
#Calculate averages of betas
lines(x = c(1,2,3),
      y = c(0, 
            mean(as.numeric(resultsFUNGsimps[,3])),
            mean(as.numeric(resultsFUNGsimps[,4]))),
      col = "black",
      lwd = 5,
      lty = 2
)

dev.off()

pdf(file = "./visuals/legendStickPlot.pdf", width = 3, height = 5)
plot(NULL,
     ylim = c(0,20),
     xlim = c(0.8,3.2),
     ylab = "",
     xpd = NA,
     cex.lab = 2,
     xlab = "",
     yaxt = "n",
     xaxt = "n",
     bty = "n"
)
legend(legend = resultsFUNGsimps[,1], col = cols, cex = 1, x = "center", lty = 1, lwd = 2)
dev.off()
# 
# ############
# # BACTERIA #
# ############
# 
# 
# dat <- dat[-which(is.na(dat$Bacterialrichness)),]
# 
# resultsBact <- matrix(ncol = 7, 
#                       nrow = 15)
# k <- 1
# colnames(resultsBact) <- c("host", "div1", "div4", "div12", "div1se", "div4se", "div12se")
# for(i in unique(dat$spp_actual)){
#   dat_sub <- subset(dat, spp_actual == i)
#   if(length(unique(dat_sub$diversity)) > 1){
#     if(length(unique(dat_sub$rep)) >= 2){
#       regR <- lm(Bacterialrichness ~ as.factor(diversity)
#                  # + ht
#                  #+ dbmem_out.MEM4
#                  # + dbmem_out.MEM3
#                  # + dbmem_out.MEM2
#                  # + dbmem_out.MEM1
#                  # + dbmem_out.MEM5
#                  # + dbmem_out.MEM6
#                  # + dbmem_out.MEM7
#                  # + dbmem_out.MEM8
#                  # + dbmem_out.MEM9
#                  #  + dbmem_out.MEM10
#                  + rep
#                  , 
#                  data = dat_sub)
#     }else{
#       regR <- lm(Bacterialrichness ~ as.factor(diversity)
#                  # + ht
#                  #+ dbmem_out.MEM4
#                  # + dbmem_out.MEM3
#                  # + dbmem_out.MEM2
#                  # + dbmem_out.MEM1
#                  # + dbmem_out.MEM5
#                  # + dbmem_out.MEM6
#                  # + dbmem_out.MEM7
#                  # + dbmem_out.MEM8
#                  # + dbmem_out.MEM9
#                  #  + dbmem_out.MEM10
#                  #+ rep
#                  , 
#                  data = dat_sub)
#     }
#   }else{next}
#   #Extract intercept and add it to the diversity estimates 
#   div1 <- as.numeric(coef(summary(regR))[,1][1])
#   div4 <- div1 + as.numeric(coef(summary(regR))[,1][2])
#   div12 <- div1 + as.numeric(coef(summary(regR))[,1][3])
#   
#   #Extract SE for each
#   div1se <- as.numeric(coef(summary(regR))[,2][1])
#   div4se <- as.numeric(coef(summary(regR))[,2][2])
#   div12se <- as.numeric(coef(summary(regR))[,2][3])
#   
#   resultsBact[k,] <- c(as.character(i), div1, div4, div12, div1se, div4se, div12se)
#   k <- k + 1
#   
# }
# 
# #Bacterial Div Shannon
# resultsBactS <- matrix(ncol = 7, 
#                        nrow = 15)
# k <- 1
# colnames(resultsBactS) <- c("host", "div1", "div4", "div12", "div1se", "div4se", "div12se")
# for(i in unique(dat$spp_actual)){
#   dat_sub <- subset(dat, spp_actual == i)
#   if(length(unique(dat_sub$diversity)) > 1){
#     if(length(unique(dat_sub$rep)) >= 2){
#       regR <- lm(exp(BacterialdiversityShannon) ~ as.factor(diversity)
#                  # + ht
#                  #+ dbmem_out.MEM4
#                  # + dbmem_out.MEM3
#                  # + dbmem_out.MEM2
#                  # + dbmem_out.MEM1
#                  # + dbmem_out.MEM5
#                  # + dbmem_out.MEM6
#                  # + dbmem_out.MEM7
#                  # + dbmem_out.MEM8
#                  # + dbmem_out.MEM9
#                  #  + dbmem_out.MEM10
#                  + rep
#                  , 
#                  data = dat_sub)
#     }else{
#       regR <- lm(exp(BacterialdiversityShannon) ~ as.factor(diversity)
#                  # + ht
#                  #+ dbmem_out.MEM4
#                  # + dbmem_out.MEM3
#                  # + dbmem_out.MEM2
#                  # + dbmem_out.MEM1
#                  # + dbmem_out.MEM5
#                  # + dbmem_out.MEM6
#                  # + dbmem_out.MEM7
#                  # + dbmem_out.MEM8
#                  # + dbmem_out.MEM9
#                  #  + dbmem_out.MEM10
#                  #+ rep
#                  , 
#                  data = dat_sub)
#     }
#   }else{next}
#   #Extract intercept and add it to the diversity estimates 
#   div1 <- as.numeric(coef(summary(regR))[,1][1])
#   div4 <- div1 + as.numeric(coef(summary(regR))[,1][2])
#   div12 <- div1 + as.numeric(coef(summary(regR))[,1][3])
#   
#   #Extract SE for each
#   div1se <- as.numeric(coef(summary(regR))[,2][1])
#   div4se <- as.numeric(coef(summary(regR))[,2][2])
#   div12se <- as.numeric(coef(summary(regR))[,2][3])
#   
#   resultsBactS[k,] <- c(as.character(i), div1, div4, div12, div1se, div4se, div12se)
#   k <- k + 1
#   
# }
# 
# 
# #Bacterial Div Shannon
# resultsBactSimpsons <- matrix(ncol = 7, 
#                               nrow = 15)
# k <- 1
# colnames(resultsBactSimpsons) <- c("host", "div1", "div4", "div12", "div1se", "div4se", "div12se")
# for(i in unique(dat$spp_actual)){
#   dat_sub <- subset(dat, spp_actual == i)
#   if(length(unique(dat_sub$diversity)) > 1){
#     if(length(unique(dat_sub$rep)) >= 2){
#       regR <- lm(1 /BacterialdiversitySimpson ~ as.factor(diversity)
#                  # + ht
#                  #+ dbmem_out.MEM4
#                  # + dbmem_out.MEM3
#                  # + dbmem_out.MEM2
#                  # + dbmem_out.MEM1
#                  # + dbmem_out.MEM5
#                  # + dbmem_out.MEM6
#                  # + dbmem_out.MEM7
#                  # + dbmem_out.MEM8
#                  # + dbmem_out.MEM9
#                  #  + dbmem_out.MEM10
#                  + rep
#                  , 
#                  data = dat_sub)
#     }else{
#       regR <- lm(1 /BacterialdiversitySimpson ~ as.factor(diversity)
#                  # + ht
#                  #+ dbmem_out.MEM4
#                  # + dbmem_out.MEM3
#                  # + dbmem_out.MEM2
#                  # + dbmem_out.MEM1
#                  # + dbmem_out.MEM5
#                  # + dbmem_out.MEM6
#                  # + dbmem_out.MEM7
#                  # + dbmem_out.MEM8
#                  # + dbmem_out.MEM9
#                  #  + dbmem_out.MEM10
#                  #+ rep
#                  , 
#                  data = dat_sub)
#     }
#   }else{next}
#   #Extract intercept and add it to the diversity estimates 
#   div1 <- as.numeric(coef(summary(regR))[,1][1])
#   div4 <- div1 + as.numeric(coef(summary(regR))[,1][2])
#   div12 <- div1 + as.numeric(coef(summary(regR))[,1][3])
#   
#   #Extract SE for each
#   div1se <- as.numeric(coef(summary(regR))[,2][1])
#   div4se <- as.numeric(coef(summary(regR))[,2][2])
#   div12se <- as.numeric(coef(summary(regR))[,2][3])
#   
#   resultsBactSimpsons[k,] <- c(as.character(i), div1, div4, div12, div1se, div4se, div12se)
#   k <- k + 1
#   
# }
# 
# 
# ########
# pdf(width = 12,
#     height = 8, 
#     file = "./visuals/BacterialDiversityLM_stickPlot.pdf")
# par(mfrow = c(1,3),
#     oma = c(4,4,1,1),
#     mar = c(2,2,1,1))
# plot(NULL,
#      ylim = c(0,150),
#      xlim = c(0.8,3.2),
#      ylab = "",
#      xpd = NA,
#      cex.lab = 2,
#      xlab = "",
#      yaxt = "n",
#      xaxt = "n",
#      bty = "n"
# )
# mtext("Richness", 
#       side = 3, 
#       line = -3,
#       cex = 2)
# axis(side = 1, labels = c("1","4","12"),
#      at = c(1,2,3),
#      cex.axis = 1.5)
# axis(side = 2, labels = seq(0,150, by = 50),
#      at = seq(0,150, by = 50),
#      las = 2,
#      cex.axis = 1.5)
# 
# for(i in 1:length(resultsBact[,1])){
#   
#   lines(x = c(1,2,3),
#         y = as.numeric(resultsBact[i,2:4]),
#         col = cols[i],
#         lwd = 2.5
#   )
#   
#   # #plot CIs, first make xy coords for polygons
#   # tops <- cbind(c(1,2,3), as.numeric(results[i,2:4]) + 1.96*as.numeric(results[i,5:7]))
#   # bottoms <- cbind(c(1,2,3), as.numeric(results[i,2:4]) - 1.96*as.numeric(results[i,5:7]))
#   # bottoms <- bottoms[nrow(bottoms):1,] #This reversal is so the polygon plots right
#   # 
#   # coords <- rbind(tops,
#   #                 bottoms)
#   # polygon(coords,
#   #         col = add.alpha(cols[i],0.1),
#   #         border = NA)
#   
# }
# #Shannons DIVERSITY
# 
# plot(NULL,
#      ylim = c(0,50),
#      xlim = c(0.8,3.2),
#      ylab = "",
#      xpd = NA,
#      cex.lab = 2,
#      xlab = "",
#      yaxt = "n",
#      xaxt = "n",
#      bty = "n"
# )
# mtext("Shannon-Weiner", 
#       side = 3, 
#       line = -3,
#       cex = 2)
# axis(side = 1, labels = c("1","4","12"),
#      at = c(1,2,3),
#      cex.axis = 1.5)
# axis(side = 2, 
#      labels = seq(0,50, by = 10),
#      at = seq(0,50, by = 10),
#      las = 2,
#      cex.axis = 1.5)
# 
# for(i in 1:length(resultsBactS[,1])){
#   
#   lines(x = c(1,2,3),
#         y = as.numeric(resultsBactS[i,2:4]),
#         col = cols[i],
#         lwd = 2.5
#   )
#   
#   # #plot CIs, first make xy coords for polygons
#   # tops <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) + 1.96*as.numeric(resultsS[i,5:7]))
#   # bottoms <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) - 1.96*as.numeric(resultsS[i,5:7]))
#   # bottoms <- bottoms[nrow(bottoms):1,] #This reversal is so the polygon plots right
#   # 
#   # coords <- rbind(tops,
#   #                 bottoms)
#   # polygon(coords,
#   #         col = add.alpha(cols[i],0.1),
#   #         border = NA)
#   
# }
# #Simpsons DIVERSITY
# 
# plot(NULL,
#      ylim = c(0,2),
#      xlim = c(0.8,3.2),
#      ylab = "",
#      xpd = NA,
#      cex.lab = 2,
#      xlab = "",
#      yaxt = "n",
#      xaxt = "n",
#      bty = "n"
# )
# mtext("Simpson", 
#       side = 3, 
#       line = -3,
#       cex = 2)
# axis(side = 1, labels = c("1","4","12"),
#      at = c(1,2,3),
#      cex.axis = 1.5)
# axis(side = 2, 
#      labels = seq(0,2, by = 1),
#      at = seq(0,2, by = 1),
#      las = 2,
#      cex.axis = 1.5)
# 
# for(i in 1:length(resultsBactSimpsons[,1])){
#   
#   lines(x = c(1,2,3),
#         y = as.numeric(resultsBactSimpsons[i,2:4]),
#         col = cols[i],
#         lwd = 2.5
#   )
#   
#   # #plot CIs, first make xy coords for polygons
#   # tops <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) + 1.96*as.numeric(resultsS[i,5:7]))
#   # bottoms <- cbind(c(1,2,3), as.numeric(resultsS[i,2:4]) - 1.96*as.numeric(resultsS[i,5:7]))
#   # bottoms <- bottoms[nrow(bottoms):1,] #This reversal is so the polygon plots right
#   # 
#   # coords <- rbind(tops,
#   #                 bottoms)
#   # polygon(coords,
#   #         col = add.alpha(cols[i],0.1),
#   #         border = NA)
#   
# }
# dev.off()
