#J. Harrison
set.seed(666)
options(scipen = 999)
rm(list=ls())

##################################
# Load packages and wrangle data #
##################################
library(lmerTest)
library(MuMIn) #for glmm r squared

formatter <- function(reg){
  #reg is a regression object
  
  #first determine significance
  print("Doublecheck these are the p values! If not, change indexing in function")
  print(summary(reg)$coefficients[,5])
  sigs <- vector()
  k <- 1
  for(i in 1:length(summary(reg)$coefficients[,5])){
    sigs[k] <- if(summary(reg)$coefficients[i,5] < 0.1) "*" else ""
    if(length(which(summary(reg)$coefficients[i,5] < 0.05)>0)){
      sigs[k] <- "**" 
    }
    if(length(which(summary(reg)$coefficients[i,5] < 0.01)>0)){
      sigs[k] <- "***" 
    }
    k <- k + 1
  }
  
  paste(round(summary(reg)$coefficients[,1],1), 
        sigs,
        " (", 
        round(summary(reg)$coefficients[,1] - 1.96*summary(reg)$coefficients[,2],1),
        ",",
        round(summary(reg)$coefficients[,1] + 1.96*summary(reg)$coefficients[,2],1),
        ")",
        sep=""
  )
}

dat <- read.csv("data/sample_metadata_corrected.7.20.19.csv")

todo <- list(dat$Fungalrichness,
      exp(dat$FungaldiversityShannon),
      1/dat$FungaldiversitySimpson)

#Bacterialrichness
#BacterialdiversityShannon
#BacterialdiversitySimpson)

pairs(data.frame(as.factor(dat$diversity),
                             scale(dat$ht, center =T, scale = T)
                          , scale(dat$dbmem_out.MEM1, center =T, scale = T)
                           , scale(dat$dbmem_out.MEM2, center =T, scale = T)
                           , scale(dat$dbmem_out.MEM3, center =T, scale = T)) )

results <- matrix(nrow = 3, 
                  ncol = 10)
k <- 1
for(i in todo){
  #This lmer is to extract overall coefficients
  regR <- lmer(i
             ~
                scale(ht, center =T, scale = T)
             + scale(dbmem_out.MEM1, center =T, scale = T)
             + scale(dbmem_out.MEM2, center =T, scale = T)
             + scale(dbmem_out.MEM3, center =T, scale = T)
            
            #proportion emf and amf add up to one, so only include one at at time
            + which_mf*as.factor(diversity)
            # + scale(dat$prop_emf, center =T, scale = T)
           
            #+ scale(dat$prop_amf, center =T, scale = T)*as.factor(diversity)
            #+ spp_actual
            #+ as.factor(diversity)*spp_actual
            #+ rep
            # + spp_actual
            + (1|rep:spp_actual),
             data = dat)
  summary(regR)
  results[k,] <- formatter(regR)
  k <- k + 1
}

#observe distribution of residuals. Need to do this for each model. 
#Rich and Shannon's are reasonable, Simpson's deviates from normality
#to a higher degree. 

plot(regR)
qqnorm(residuals(regR))

#determine influence of outliers
library(ggplot2)
ggplot(data.frame(lev=hatvalues(regR),pearson=residuals(regR,type="pearson")),
       aes(x=lev,y=pearson)) +
  geom_point() +
  theme_bw()
hist(hatvalues(regR) )
levId <- which(hatvalues(regR) >= 
               #  0.09 #richness
                # 0.08 #shannon
                0.06 #simpson
               )

regRno_Out <- lmer(i[-levId] ~ as.factor(diversity) +
               scale(ht, center =T, scale = T)
             + scale(dbmem_out.MEM1, center =T, scale = T)
             + scale(dbmem_out.MEM2, center =T, scale = T)
             + scale(dbmem_out.MEM3, center =T, scale = T)
             + which_mf
             + (1|rep:spp_actual),
             data = dat[-levId,])
summary(regRno_Out)
qqnorm(residuals(regRno_Out))
plot(regRno_Out)

colnames(results) <- c("Intercept",
                       "Height",
                       
                       "MEM 1",
                       "MEM 2",
                       "MEM 3",
                       "ECM",
                       "Diversity = 4",
                       "Diversity = 12",
                       "ECM*Diversity = 4",
                       "ECM*Diversity = 12"
)
rownames(results) <- c("Fungal richness",
                       "exp(Shannon)",
                       "1/Simpsons")
write.csv(results, file = "./biodiversitree_results/lm_results_with_interaction_mf_div.csv")

