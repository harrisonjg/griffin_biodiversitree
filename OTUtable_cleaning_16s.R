#J. Harrison
#Oct 22, 2018

#Load data

options(scipen = 999)

otus <- read.table("./data/bacteria/otuTableZotus.txt",
                   header= T)


tax <- read.table("./data/bacteria/ESVcombinedTaxonomy.txt", 
                  fill = T)
unwanted_cp_mtDNA <-
  read.table("./data/bacteria/possible_mt_cpDNAzotus/allhits.b6")

#remove unwanted cp and mt DNA
dim(otus)
otus <- otus[-which(as.character(otus$OTU_ID) %in% as.character(unwanted_cp_mtDNA$V1)), ]
dim(otus)
#confirm they are all unwanted, they are
tax[which(as.character(tax[,1]) %in% as.character(unwanted_cp_mtDNA$V1)),]

#colSums(otus[,2:length(otus)])

#check that taxonomy and otus are in same order
#Note that the script does not depend on order, so this doesn't matter
#cbind(otus$OTU_ID, tax[,1])

#########################
#remove remaining cp DNA#
#########################

#Note I quickly scanned through the output of grep to make sure
#I want to remove all these taxa. Do not remove cpDNA via this
#command without searching through results first, as sometimes
#OTUs are assigned to cpDNA with very low certainty, and
#should be retained within the data.

#Use this to review the results of grep
tax$V4[grep("Cyanobacteria/Chloroplast",as.character(tax$V4))]

dim(otus)
otus <- otus[-which(otus$OTU_ID %in% as.character(tax$V1[grep("Cyanobacteria/Chloroplast",
                                                          tax$V4)])), ]
dim(otus) #removed 1786-1302 more OTUs

################
# Remove mtDNA #
################

mitos <- c(as.character(tax$V1[grep("[Mm]itochondria", as.character(tax$V3))]),
           as.character(tax$V1[grep("[Mm]itochondria", as.character(tax$V4))]))
cpDNA <- c(as.character(tax$V1[grep("[Cc]hloroplast", as.character(tax$V3))]),
          as.character(tax$V1[grep("[Cc]hloroplast", as.character(tax$V4))]))
tormv <- c(as.character(mitos), 
           as.character(cpDNA))

#remove the bogus OTUs
dim(otus)
otus <- otus[-which(as.character(otus$OTU_ID) %in% tormv),]
dim(otus)

#Get into a better format.
t_otus <- as.data.frame(t(otus[,2:length(otus)]))

names(t_otus) <- otus$OTU_ID

#Replace rownames with actual sample names. 
t_otus$samps <- gsub("(\\w+\\d+\\.\\d+)_S\\d+_L\\d+_R1_001.fastq", 
                        "\\1", 
                        names(otus))[-1]
# A few duplicates.
dupes <- t_otus[which(duplicated(t_otus$samps )),length(t_otus)]
#compare them to their originals

totest <- which(t_otus$samps %in% dupes)

props <- matrix(nrow = length(totest), 
                ncol = length(t_otus) - 1)
k <- 1
for(i in totest){
  props[k,] <- unlist(t_otus[i, 1:(length(t_otus)-1)] / sum(t_otus[i, 1:(length(t_otus)-1)]))
  k <- k + 1
}
dupesdf <- data.frame(t_otus$samps[totest], props[,which(colSums(props) > 0 )])
dupesdf[,1:100]
#SANITYCHECK
rowSums(props)
#Some variation in technical replicates, but not too terrible. Rarely more than 10% change. This would 
#Change into a nearly nonexistent change if plant DNA was included in calculations.

#Odd that one rep has hardly anything in it. Keep just the more abundant reps. 
rowSums(t_otus[totest,1:(length(t_otus) - 1)])

totest #use this to figure out which to cut
t_otus <- t_otus[-c(72,142,216,288,359,431),]

#rename for convenience
dat <- t_otus

info <- read.csv("./data/sampleinfo.csv")
info$label <- paste(info$column, info$well,".", info$Plate, sep="")
dim(info)

#Check that there are not extra samples that were sequenced that are not in
#labels. 

#indeed there were. These include negative controls and a variety of H* samples
#That Eric said were haphazardly collected samples that shoul be discarded

setdiff(dat$samps,info$label)

#Determine what these samples are
orphaned <- dat[which(dat$samps %in% setdiff(dat$samps, info$label)),length(dat)] 

#remove the H samples from the data. 
Hs <- orphaned[grep("^H", as.character(orphaned))]
tormv <- gsub("(\\w+\\d+\\.\\d+)_S\\d+_L\\d+_R1_001.fastq", 
     "\\1", 
     as.character(Hs))

dat <- dat[-which(dat$samps %in% tormv),]

#Sum up controls
contams <- colSums(dat[grep("TRL", dat$samps),2:(length(dat)-1)])
contams2 <- colSums(dat[which(dat$samps == "H12.4"),2:(length(dat)-1)])
#Eric said 12.4 was a summed contaminant sample, however it does not seem to 
#be as complete as the other contaminant samples, so I will not consider it here. 
cbind(contams, contams2)

#Determine which of the putative contaminants occur at 1% or better in the 
#negative controls. Note we do not include the negative controls in the sum.
dim(dat[-grep("TRL", dat$samps),2:(length(dat)-1)])
tormv <- names(which((contams / colSums(dat[-grep("TRL", dat$samps), 2:(length(dat)-1)])) > 0.01))

dat <- dat[,-which(names(dat) %in% tormv)]

#remove neg controls
dat <- dat[-grep("TRL", dat$samps),]

#changed field 1 header to samps in excel after the fact
write.csv(data.frame(dat$samps, dat[,1:(length(dat)-1)]), 
                     file = "./data/bacteria/clean_zotuTable_16s.csv",
          row.names = F)



