#J. Harrison
set.seed(666)
options(scipen = 999)
rm(list=ls())

##################################
# Load packages and wrangle data #
##################################
library(lmerTest)
library(MuMIn) #for glmm r squared

formatter <- function(reg){
  #reg is a regression object
  
  #first determine significance
  print("Doublecheck these are the p values! If not, change indexing in function")
  print(summary(reg)$coefficients[,5])
  sigs <- vector()
  k <- 1
  for(i in 1:length(summary(reg)$coefficients[,5])){
    sigs[k] <- if(summary(reg)$coefficients[i,5] < 0.1) "*" else ""
    if(length(which(summary(reg)$coefficients[i,5] < 0.05)>0)){
      sigs[k] <- "**" 
    }
    if(length(which(summary(reg)$coefficients[i,5] < 0.01)>0)){
      sigs[k] <- "***" 
    }
    k <- k + 1
  }
  
  paste(round(summary(reg)$coefficients[,1],1), 
        sigs,
        " (", 
        round(summary(reg)$coefficients[,1] - 1.96*summary(reg)$coefficients[,2],1),
        ",",
        round(summary(reg)$coefficients[,1] + 1.96*summary(reg)$coefficients[,2],1),
        ")",
        sep=""
  )
}

#LOAD
pd <- read.csv("./data/2017_trophic_tree_PlotLevel_PDwithSES_richness.csv")
dat <- read.csv("data/sample_metadata_corrected.7.20.19.csv")

#first calculate average richness/diversity per plot

shan <- aggregate(dat$FungaldiversityShannon~dat$plot, FUN=mean)
simps <- aggregate(dat$FungaldiversitySimpson~dat$plot, FUN=mean)
rich <- aggregate(dat$Fungalrichness~dat$plot, FUN=mean)
mem1 <- aggregate(dat$dbmem_out.MEM1~dat$plot, FUN=mean)
mem2 <- aggregate(dat$dbmem_out.MEM2~dat$plot, FUN=mean)
mem3 <- aggregate(dat$dbmem_out.MEM3~dat$plot, FUN=mean)
ht <- aggregate(dat$ht~dat$plot, FUN=mean)

dat$rep <- as.character(dat$rep)
dat$rep[dat$rep=="rep1"] <- 1
dat$rep[dat$rep=="rep2"] <- 2
dat$rep <- as.numeric(dat$rep) #ugh

rep <- aggregate(dat$rep~dat$plot, FUN=mean)

dim(pd)
pd <- merge(pd, shan, by.x = "plot", by.y ="dat$plot", all.x = T)
pd <- merge(pd, simps, by.x = "plot", by.y ="dat$plot", all.x = T)
pd <- merge(pd, rich, by.x = "plot", by.y ="dat$plot", all.x = T)
pd <- merge(pd, mem1, by.x = "plot", by.y ="dat$plot", all.x = T)
pd <- merge(pd, mem2, by.x = "plot", by.y ="dat$plot", all.x = T)
pd <- merge(pd, mem3, by.x = "plot", by.y ="dat$plot", all.x = T)
pd <- merge(pd, ht, by.x = "plot", by.y ="dat$plot", all.x = T)
pd <- merge(pd, rep, by.x = "plot", by.y ="dat$plot", all.x = T)
dim(pd)
cor.test(pd$pd.ses, pd$`dat$FungaldiversityShannon`)

dim(dat)
todo <- list(pd$`dat$Fungalrichness`,
             exp(pd$`dat$FungaldiversityShannon`),
             1/pd$`dat$FungaldiversitySimpson`)

results <- matrix(nrow = 3, 
                  ncol = 6)

tormv <- which(is.na(todo[[1]]))

k <- 1
for(i in todo){
  #This lmer is to extract overall coefficients
  regR <- lmer(i[-tormv] ~ scale(pd$pd.ses[-tormv], center = T, scale= T)
              + scale(pd$`dat$ht`[-tormv], center = T, scale= T)
               + scale(pd$`dat$dbmem_out.MEM1`[-tormv], center = T, scale= T)
               + scale(pd$`dat$dbmem_out.MEM2`[-tormv], center = T, scale= T)
               + scale(pd$`dat$dbmem_out.MEM3`[-tormv], center = T, scale= T)
               +(1|pd$`dat$rep`[-tormv]))
  summary(regR)
  results[k,] <- formatter(regR)
  k <- k + 1
}

pdsesz <- scale(pd$pd.ses[-tormv], center = T, scale= T)


htz <-scale(pd$`dat$ht`[-tormv], center = T, scale= T)
mem1z <-  scale(pd$`dat$dbmem_out.MEM1`[-tormv], center = T, scale= T)
mem2z <- scale(pd$`dat$dbmem_out.MEM2`[-tormv], center = T, scale= T)
mem3z <- scale(pd$`dat$dbmem_out.MEM3`[-tormv], center = T, scale= T)


rep <- pd$`dat$rep`[-tormv]
spp_actual <- dat$spp_actual[-tormv]
efr <- pd$`dat$Fungalrichness`[-tormv]
efd <- exp(pd$`dat$FungaldiversityShannon`[-tormv])


reg <- lmerTest::lmer(efr ~ pdsesz
                      + htz
                      + mem1z
                      + mem2z
                      + mem3z
                      + (1|rep))#,
summary(reg)

regdiv <- lmerTest::lmer(efd ~ pdsesz
                         + htz
                         + mem1z
                         + mem2z
                         + mem3z
                         + (1|rep))#,

#partial regression plot

library(effects);
PARTIAL_MODEL <- Effect('pdsesz', partial.residuals = T, mod = reg);

pdf(file = "./visuals/phyloPartialPlot_PLOTLEVEL.pdf", width = 6, height = 6)
par(mfrow = c(2,1))
plot(PARTIAL_MODEL, main = '',
     xlab = 'Phylogenetic diversity', ylab = 'Richness' , multiline = F, smooth.residuals = F)

PARTIAL_MODEL <- Effect('pdsesz', partial.residuals = T, mod = regdiv);

plot(PARTIAL_MODEL, main = '',
     xlab = 'Phylogenetic diversity', ylab = "Shannon's equivalents" , 
     multiline = F, smooth.residuals = F)

dev.off()



library(effects);
PARTIAL_MODEL <- Effect('scale(pd$pd.ses[-tormv], center = T, scale = T)', 
                        partial.residuals = T, mod = regR);

pdf(file = "./visuals/phyloPartialPlot_PlotLevel.pdf", width = 6, height = 6)
par(mfrow = c(2,1))
plot(PARTIAL_MODEL, main = '',
     xlab = 'Phylogenetic diversity', ylab = 'Richness' , multiline = F, smooth.residuals = F)

PARTIAL_MODEL <- Effect('pdsesz', partial.residuals = T, mod = regdiv);

plot(PARTIAL_MODEL, main = '',
     xlab = 'Phylogenetic diversity', ylab = "Shannon's equivalents" , 
     multiline = F, smooth.residuals = F)

dev.off()

write.csv(results, file = "~/Desktop/results.csv")
