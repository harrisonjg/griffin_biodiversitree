dat <- read.csv("./data/bacteria/clean_zotuTable.csv")
dim(dat)
rowSums(dat[,2:length(dat)])
#names go column, row, plate

info <- read.csv("./data/sampleinfo.csv")
info$label <- paste(info$column, info$well,".", info$Plate, sep="")
info <- info[-c(556:length(info[,1])),]
dim(info) # extra samples here, what be?

dat$samps <- gsub("(\\w+\\d+\\.\\d+)_S\\d+_L\\d+_R1_001.fastq", 
                  "\\1", 
                  as.character(dat[,1]))

#These samples were not sequenced
info[which(info$label %in% setdiff(info$label, dat$samps)),]

#Check that there are not extra samples that were sequenced that are not in
#labels. 

#indeed there were
setdiff( dat$samps,info$label)

#Determine what these samples are
dat[which(dat$samps %in% setdiff(dat$samps, info$label)),1:5] 

rowSums(dat[which(dat$samps %in% setdiff(dat$samps, info$label)), 2:(length(dat)-1)])



datmerge <- merge(info, dat, by.y="samps", by.x="label")
dim(datmerge)

#stolen from Mage
add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2, 
        function(x) 
          rgb(x[1], x[2], x[3], alpha=alpha))  
}

########
#rarefy#
########

datrare15k <- vegan::rrarefy(datmerge[,16:(length(datmerge)-4)],1000)

datmerge$Fungalrichness<- NA
datmerge$FungaldiversityShannon <- NA
datmerge$FungaldiversitySimpson <- NA

for(i in 1:length(datrare15k[,1])){
  datmerge$Fungalrichness[i] <- length(which(datrare15k[i,]>0))
  datmerge$FungaldiversityShannon[i] <- vegan::diversity(datrare15k[i,], index="shannon")
  datmerge$FungaldiversitySimpson[i] <- vegan::diversity(datrare15k[i,], index="simpson")
}

##########################################################################
#make a three row series of boxplots, with each spp. shown in each plot.##
#do this for rich and div.
##########################################################################

#define and assign colors
coul <- colorRampPalette(RColorBrewer::brewer.pal(8,"Set1"))(16)
#pie(rep(1, length(coul)), col = coul , main="") 

datmerge$col <- NA
for(i in 1:16){
  datmerge$col[which(datmerge$spp..actual. == levels(datmerge$spp..actual.)[i])] <- coul[i]
}

div1 <- datmerge[which(datmerge$diversity==1),]
div4 <- datmerge[which(datmerge$diversity==4),]
div12 <- datmerge[which(datmerge$diversity==12),]

#make ordering factor 
datmerge <- datmerge[order(as.numeric(as.character(datmerge$diversity))),]
datmerge <- datmerge[order(datmerge$spp_actual),]
datmerge$orderingField <- paste(datmerge$spp_actual, datmerge$diversity, sep="")
datmerge$orderingField <- as.factor(datmerge$orderingField)

#the stupid factors are not in right order. 
datmerge$orderingField <- factor(datmerge$orderingField, levels(datmerge$orderingField)[unique(datmerge$orderingField)])

#make colors
colorvec <- NA
for(i in 1:length(unique(datmerge$col))){
colorvec <- c(colorvec, rep(add.alpha(unique(datmerge$col)[i], alpha=0.4),3))
}

colorvec <- colorvec[-1]

#begin plotting

pdf(width=8,
     height=10,
     file = "./visuals/16srichnessBoxplotVsDiversity.pdf")
par(mfrow=c(3,1), 
    mar=c(3,5,0,0),
    oma=c(4,3,0,0))
foci <- which(datmerge$orderingField %in% unique(datmerge$orderingField)[1:15])

stripchart(datmerge$Fungalrichness[foci]~
          datmerge$orderingField[foci],
           vertical = TRUE, 
           data = datmerge, 
           method = "jitter", 
           pch = 20, 
           col = colorvec,
           cex=2.5,
           xaxt="n",
           yaxt="n",
           ylab="",
           xlim=c(0.5,15.5),
           ylim=c(0,400),
           frame.plot = FALSE
           )

axis(side=2,
     at=c(seq(0,400,by=100)),
      las=2)
boxplot(datmerge$Fungalrichness[foci]~
         droplevels(datmerge$orderingField[foci]),
        las=2,
        yaxt="n",
        #xaxt="n",
        outline=F,
        add=T, 
        col = colorvec,
        frame.plot = FALSE
)

#plot 2

foci <- which(datmerge$orderingField %in% unique(datmerge$orderingField)[16:30])

stripchart(datmerge$Fungalrichness[foci]~
          droplevels(datmerge$orderingField[foci]),
           vertical = TRUE, 
           data = datmerge, 
           method = "jitter", 
           pch = 20, 
           col = colorvec[16:30],
           cex=2.5,
           xaxt="n",
           yaxt="n",
           ylab="",
           xlim=c(0.5,15.5),
           ylim=c(0,400),
           frame.plot = FALSE
           )

axis(side=2,
     at=c(seq(0,400,by=100)),
      las=2)
boxplot(datmerge$Fungalrichness[foci]~
         droplevels(datmerge$orderingField[foci]),
        las=2,
        yaxt="n",
        #xaxt="n",
        ylab="Richness",
        outline=F,
        add=T, 
        cex.lab=3,
        col = colorvec[16:30],
        frame.plot = FALSE
)

#plot 3

foci <- which(datmerge$orderingField %in% unique(datmerge$orderingField)[31:45])

stripchart(datmerge$Fungalrichness[foci]~
          droplevels(datmerge$orderingField[foci]),
           vertical = TRUE, 
           data = datmerge, 
           method = "jitter", 
           pch = 20, 
           col = colorvec[31:45],
           cex=2.5,
           xaxt="n",
           yaxt="n",
           ylab="",
           xlim=c(0.5,15.5),
           ylim=c(0,400),
           frame.plot = FALSE
           )

axis(side=2,
     at=c(seq(0,400,by=100)),
      las=2)
boxplot(datmerge$Fungalrichness[foci]~
         droplevels(datmerge$orderingField[foci]),
        las=2,
        yaxt="n",
        #xaxt="n",
        outline=F,
        add=T, 
        col = colorvec[31:45],
        frame.plot = FALSE
)
dev.off()

#################
#plot diversity#
#################
pdf(width=8,
     height=10,
     file = "./visuals/16sShannonsBoxplotVsDiversity.pdf")
par(mfrow=c(3,1), 
    mar=c(3,5,0,0),
    oma=c(4,3,0,0))
foci <- which(datmerge$orderingField %in% unique(datmerge$orderingField)[1:15])

stripchart(datmerge$FungaldiversityShannon[foci]~
          datmerge$orderingField[foci],
           vertical = TRUE, 
           data = datmerge, 
           method = "jitter", 
           pch = 20, 
           col = colorvec,
           cex=2.5,
           xaxt="n",
           yaxt="n",
           ylab="",
           xlim=c(0.5,15.5),
           ylim=c(0,4.2),
           frame.plot = FALSE
           )

axis(side=2,
     at=c(seq(0,4,by=1)),
      las=2)
boxplot(datmerge$FungaldiversityShannon[foci]~
         droplevels(datmerge$orderingField[foci]),
        las=2,
        yaxt="n",
        #xaxt="n",
        outline=F,
        add=T, 
        col = colorvec,
        frame.plot = FALSE
)

#plot 2

foci <- which(datmerge$orderingField %in% unique(datmerge$orderingField)[16:30])

stripchart(datmerge$FungaldiversityShannon[foci]~
          droplevels(datmerge$orderingField[foci]),
           vertical = TRUE, 
           data = datmerge, 
           method = "jitter", 
           pch = 20, 
           col = colorvec[16:30],
           cex=2.5,
           xaxt="n",
           yaxt="n",
           ylab="",
           xlim=c(0.5,15.5),
           ylim=c(0,4.2),
           frame.plot = FALSE
           )

axis(side=2,
     at=c(seq(0,4,by=1)),
      las=2)
boxplot(datmerge$FungaldiversityShannon[foci]~
         droplevels(datmerge$orderingField[foci]),
        las=2,
        yaxt="n",
        #xaxt="n",
        ylab="Shannon's diversity",
        outline=F,
        add=T, 
        cex.lab=2.7,
        col = colorvec[16:30],
        frame.plot = FALSE
)

#plot 3

foci <- which(datmerge$orderingField %in% unique(datmerge$orderingField)[31:45])

stripchart(datmerge$FungaldiversityShannon[foci]~
          droplevels(datmerge$orderingField[foci]),
           vertical = TRUE, 
           data = datmerge, 
           method = "jitter", 
           pch = 20, 
           col = colorvec[31:45],
           cex=2.5,
           xaxt="n",
           yaxt="n",
           ylab="",
           xlim=c(0.5,15.5),
           ylim=c(0,4.2),
           frame.plot = FALSE
           )

axis(side=2,
     at=c(seq(0,4,by=1)),
      las=2)
boxplot(datmerge$FungaldiversityShannon[foci]~
         droplevels(datmerge$orderingField[foci]),
        las=2,
        yaxt="n",
        #xaxt="n",
        outline=F,
        add=T, 
        col = colorvec[31:45],
        frame.plot = FALSE
)
dev.off()



# 
# pdf(width=8,heightfile = "./visuals/richnessBoxplot.pdf")
# par(mfrow=c(3,1), mar=c(2,3,0,1), oma=c(4,0,0,0))
# #div 12 plot
# stripchart(div12$Fungalrichness~droplevels(div12$spp..actual.),
#            vertical = TRUE, 
#            data = div12, 
#            method = "jitter", 
#            pch = 20, 
#            col = unique(add.alpha(div12$col, alpha=0.4)),
#            cex=2.5,
#            xaxt="n",
#            yaxt="n",
#            ylab="",
#            xlim=c(0.5,15.5),
#            ylim=c(0,400),
#            frame.plot = FALSE
#            )
# axis(side=2,
#      at=c(seq(0,400,by=100)),
#       las=2)
# boxplot(div12$Fungalrichness~droplevels(div12$spp..actual.),
#         las=2,
#         yaxt="n",
#         xaxt="n",
#         outline=F,
#         add=T, 
#         col = unique(add.alpha(div12$col, alpha=0.4)),
#         frame.plot = FALSE
# )
# 
# 
# #div 4 plot
# stripchart(div4$Fungalrichness~droplevels(div4$spp..actual.),
#            vertical = TRUE, 
#            data = div4, 
#            method = "jitter", 
#            pch = 20, 
#            col = unique(add.alpha(div12$col, alpha=0.4)), #note this in the order of div12 on purpose. 
#            #Doesnt matter what order is, so long as it is the same across plots
#            cex=2.5,
#            xaxt="n",
#            yaxt="n",
#            ylab="",
#            xlim=c(0.5,15.5),
#            ylim=c(0,400),
#            frame.plot = FALSE
#            )
# axis(side=2,
#      at=c(seq(0,400,by=100)),
#       las=2)
# boxplot(div4$Fungalrichness~droplevels(div4$spp..actual.),
#         las=2,
#         yaxt="n",
#         xaxt="n",
#         outline=F,
#         add=T, 
#         col = unique(add.alpha(div12$col, alpha=0.4)),
#         frame.plot = FALSE
# )
# 
# 
# #div 1 plot
# stripchart(div1$Fungalrichness~droplevels(div1$spp..actual.),
#            vertical = TRUE, 
#            data = div1, 
#            method = "jitter", 
#            pch = 20, 
#            col = unique(add.alpha(div12$col, alpha=0.4)),
#            cex=2.5,
#            xaxt="n",
#            yaxt="n",
#            ylab="",
#            xlim=c(0.5,15.5),
#            ylim=c(0,400),
#            frame.plot = FALSE
#            )
# axis(side=2,
#      at=c(seq(0,400,by=100)),
#       las=2)
# boxplot(div1$Fungalrichness~droplevels(div1$spp..actual.),
#         las=2,
#         yaxt="n",
#         outline=F,
#         add=T, 
#         cex.lab=2,
#         col = unique(add.alpha(div12$col, alpha=0.4)),
#         frame.plot = FALSE
# )
# dev.off()
# 


